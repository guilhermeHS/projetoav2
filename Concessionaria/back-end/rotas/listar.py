from settings.config import *
from classes.alugarCarro import *
from classes.carro import *
from classes.cliente import *

@app.route("/listar/<string:classe>")
def listar(classe):
    dados = None
    if classe == "AlugarCarro":
       dados = db.session.query(AlugarCarro).all()  
    elif classe == "Cliente":
       dados = db.session.query(Cliente).all()   
    elif classe == "Carro":
       dados = db.session.query(Carro).all()
    if dados:
      lista_jsons = [x.json() for x in dados]

      meujson = {"resultado": "ok"}
      meujson.update({"detalhes": lista_jsons})
      return jsonify(meujson)
    else:
      return jsonify({"resultado":"erro", "detalhes":"classe informada inválida: "+classe})

"""
Teste da classe AlugarCarro:
$curl localhost:5000/listar/AlugarCarro
{
  "detalhes": [
    {
      "carro_id": "cli1",
      "cliente_id": "cli1",
      "data": "15/03/04",
      "id": 1
    }
  ],
  "resultado": "ok"
}

Teste da classe Cliente

$curl localhost:5000/listar/Cliente
{
  "detalhes": [
    {
      "cpf": 66666666,
      "id": 1,
      "nascimento": "13/04/1988",
      "nome": "Reginaldo"
    }
  ],
  "resultado": "ok"
}

Teste da classe Carro

$curl localhost:5000/listar/Carro
{
  "detalhes": [
    {
      "ano": 1940,
      "id": 1,
      "marca": "Volkswagen",
      "modelo": "Volkswagen Fusca"
    }
  ],
  "resultado": "ok"
}

"""