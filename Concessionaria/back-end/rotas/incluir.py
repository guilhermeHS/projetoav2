from settings.config import *
from classes.alugarCarro import *
from classes.carro import *
from classes.cliente import *

@app.route("/incluir/<string:classe>", methods=['post'])
def incluir(classe):
    dados = request.get_json()  
    try:  
        nova = None
        if classe == "AlugarCarro":
            nova = AlugarCarro(**dados)
        elif classe == "Cliente":
            nova = Cliente(**dados)
        elif classe == "Carro":
            nova = Carro(**dados)
        db.session.add(nova)
        db.session.commit()
        return jsonify({"resultado": "ok", "detalhes": "ok"})
    except Exception as e:
        return jsonify({"resultado": "erro", "detalhes": str(e)})
    
# Teste de AlugarCarro:
# curl localhost:5000/incluir/AlugarCarro -X POST -d '{"data":"13/04/23", "cliente_id":"cli1", "carro_id":"car1"}' -H 'Content-Type:application/json' 
# Teste de cliente:
# curl localhost:5000/incluir/Cliente -X POST -d '{"nome":"Diogo", "nascimento":"15/04/03", "cpf":"3213122"}' -H 'Content-Type:application/json' 
# Teste de carro:
# curl localhost:5000/incluir/Carro -X POST -d '{"nome":"Onix", "marca":"chevrolet", "ano":"2024"}' -H 'Content-Type:application/json' 