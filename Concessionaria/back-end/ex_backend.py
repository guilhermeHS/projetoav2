from settings.config import *
from rotas.incluir import *
from rotas.listar import *

# rota padrão
@app.route("/")
def inicio():
    return 'Concessionaria do Gui'

# inserindo a aplicação em um contexto :-/
with app.app_context():

    app.run(debug=True, host="0.0.0.0")
