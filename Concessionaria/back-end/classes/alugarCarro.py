from settings.config import *
from classes.carro import Carro
from classes.cliente import Cliente

class AlugarCarro(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.Text)

    cliente_id = db.Column(db.Integer, db.ForeignKey(Cliente.id), nullable=False)
    cliente = db.relationship("Cliente")
    carro_id = db.Column(db.Integer, db.ForeignKey(Carro.id), nullable=False)
    carro = db.relationship("Carro")

    def __str__(self):
        return f"{self.data}, {self.cliente_id}, " + \
            f"{self.carro_id}"
    
    def json(self):
        return{
            "id":self.id,
            "data":self.data,
            "cliente_id":self.cliente_id,
            "carro_id":self.cliente_id
        }
