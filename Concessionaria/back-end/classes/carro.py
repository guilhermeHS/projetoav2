from settings.config import *

class Carro(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    nome = db.Column(db.Text)
    marca = db.Column(db.Text)
    ano = db.Column(db.Text)

    def __str__(self):
        return f"--Carro-- " + \
            f"id:{self.id}, modelo:{self.nome}, fabricante:{self.marca}, ano:{self.ano}"
    
    def json(self):
        return{
            "id":self.id,
            "nome":self.nome,
            "marca":self.marca,
            "ano":self.ano
        }