from settings.config import *

class Cliente(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text)
    nascimento = db.Column(db.Text)
    cpf = db.Column(db.Text)

    def __str__(self):
        return f"id:{self.id}, nome:{self.nome}, " + \
          f"data de nascimento: {self.nascimento}, cpf: {self.cpf}"
    
    def json(self):
        return{
            "id":self.id,
            "nome":self.nome,
            "nascimento":self.nascimento,
            "cpf":self.cpf
        }
