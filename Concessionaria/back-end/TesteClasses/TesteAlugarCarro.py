from settings.config import *
from classes.alugarCarro import *
from classes.carro import *
from classes.cliente import *

def run():
    print("Teste de Alugar Carro:")

    cli1 = Cliente(nome = "Ana Souza", nascimento="13/04/25", cpf="34388379")
    cli2 = Cliente(nome = "Marcos Almeida", nascimento="16/01/2011", cpf="9184937568")
    db.session.add(cli1)
    db.session.add(cli2)
    db.session.commit()
    print(f"Cliente:{cli1}, Cliente{cli2}")

    car1 = Carro(nome="CyberTruck", marca="Tesla", ano="2022")
    car2 = Carro(nome="Fiat Uno", marca="Fiat", ano="2004")
    db.session.add(car1)
    db.session.add(car2)
    db.session.commit()
    print(f"{car1}, {car2}")

    t1 = AlugarCarro(data="15/03/04", cliente_id="cli1", carro_id="car1")
    db.session.add(t1)
    db.session.commit()
    print(f"{t1}")
