from settings.config import *
from classes.alugarCarro import *
from classes.cliente import *
from classes.carro import *

# inserindo a aplicação em um contexto :-/
with app.app_context():

    if os.path.exists(arquivobd):
        os.remove(arquivobd)

    # criar tabelas
    db.create_all()

    print("Criação de BD e tabelas bem sucedidada")
