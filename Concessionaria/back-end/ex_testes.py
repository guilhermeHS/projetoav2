from settings.config import *
from TesteClasses import *

# inserindo a aplicação em um contexto :-/
with app.app_context():

    TesteCliente.run(),
    TesteCarros.run(),
    TesteAlugarCarro.run()
