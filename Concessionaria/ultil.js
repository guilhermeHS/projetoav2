
    // proteção para não executar o javascript antes do documento estar pronto
    $(function () {

        // conecta o botão de enviar à ação javascript/jquery
        $(document).on("click", "#btIncluir", function () {

            // rota que vai ser chamada no backend
            // rota GENÉRICA :-)
            var rota = 'http://191.52.7.81:5000/incluir/AlugarCarro';

            // obter dados form
            var vetor_dados = $("#meuform").serializeArray();

            // conversão para formato chave:valor
            var chave_valor = {};
            for (var i = 0; i < vetor_dados.length; i++) {
                chave_valor[vetor_dados[i]['name']] = vetor_dados[i]['value'];
            }

            // convertendo para JSON
            var dados_json = JSON.stringify(chave_valor);

            // chamada ajax
            var acao = $.ajax({
                url: rota,
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: dados_json
            });

            // se a chamada der certo
            acao.done(function (retorno) {
                try {
                    if (retorno.resultado == "ok") {
                        alert("Carro alugado incluído");
                    } else {
                        alert("Deu algum erro no backend: " + retorno.detalhes);
                    }
                } catch (error) {
                    alert("Erro ao tentar fazer o ajax: " + error +
                        "\nResposta da ação: " + retorno);
                }
            });

            acao.fail(function (jqXHR, textStatus) {
                mensagem = encontrarErro(jqXHR, textStatus, rota);
                alert("Erro na chamada ajax: " + mensagem);
            });
        }); // fim click btIncluir

        function carregarCombo(combo_id, nome_classe) {
            var acao = $.ajax({
                url: 'http://191.52.7.81:5000/listar/'+nome_classe,                    
                dataType: 'json', // os dados são recebidos no formato json
            });
            // se a chamada der certo
            acao.done(function (retorno) {
                try {
                    if (retorno.resultado == "ok") {
                        montar_combo(combo_id, retorno.detalhes);
                    } else {
                        alert("Deu algum erro no backend: " + retorno.detalhes);
                    }
                } catch (error) { // se algo der errado...
                    alert("Erro ao tentar fazer o ajax: " + error +
                        "\nResposta da ação: " + retorno.detalhes);
                }
            });

            // se a chamada der erro
            acao.fail(function (jqXHR, textStatus) {
                mensagem = encontrarErro(jqXHR, textStatus, rota);
                alert("Erro na chamada ajax: " + mensagem);
            });
            
            function montar_combo (combo_id, dados) {
                // esvaziar o combo
                $('#'+combo_id).empty();
                // percorrer a lista de dados
                for (var linha of dados) {
                    $('#'+combo_id).append(
                        $('<option></option>').attr("value", 
                            linha.id).text(linha.nome));
                }
            }
        }

        // carregar combo de pessoas e exames
        carregarCombo("cliente_id", "Cliente");
        carregarCombo("carro_id", "Carro");

        // função para encontrar o erro
            // https://stackoverflow.com/questions/6792878/jquery-ajax-error-function 
            function encontrarErro(jqXHR, textStatus, rota) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Não foi possível conectar, ' +
                        'verifique se o endereço do backend está certo' +
                        ' e se o backend está rodando.';
                } else if (jqXHR.status == 404) {
                    msg = 'A URL informada não foi encontrada no ' +
                        'servidor [erro 404]: ' + rota;
                } else if (jqXHR.status == 500) {
                    msg = 'Erro interno do servidor [erro 500], ' +
                        'verifique nos logs do servidor';
                } else if (textStatus === 'parsererror') {
                    msg = 'Falha ao decodificar o resultado json';
                } else if (textStatus === 'timeout') {
                    msg = 'Tempo excessivo de conexão, estourou o limite (timeout)';
                } else if (textStatus === 'abort') {
                    msg = 'Requisição abortada (abort)';
                } else {
                    msg = 'Erro desconhecido: ' + jqXHR.responseText;
                }
                return msg;
            }

    });